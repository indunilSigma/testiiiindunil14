const google = require('googleapis').google;
const _auth = require('./Authorizer');
const pubsub = google.pubsub('v1');
const AWS = require('aws-sdk');
const sqs = new AWS.SQS();

exports.handler = async (event) => {
    try {
        let data = await sqs.receiveMessage({
            QueueUrl: `https://sqs.${process.env.AWS_DEFAULT_REGION}.amazonaws.com/${process.env.SIGMA_AWS_ACC_ID}/hiru-test`,
            MaxNumberOfMessages: 2,
            VisibilityTimeout: 40,
            WaitTimeSeconds: 0,
            AttributeNames: ['All']
        }).promise();

    } catch (err) {
        // error handling goes here
    };
    pubsub.projects.topics.subscriptions.list({
        topic: `projects/${process.env.GCP_PROJECT}/topics/cloud-builds`,
        pageSize: 10
    })
        .then(response => {
            console.log(response.data);  // successful response
            /*
            response.data = {
                "subscriptions": [
                    "projects/<project>/subscriptions/<subscription-1>",
                    "projects/<project>/subscriptions/<subscription-2>",
                    ...
                ]
            }
            */
        })
        .catch(err => {
            console.log(err, err.stack); // an error occurred
        });


    return { "message": "Successfully executed" };
};